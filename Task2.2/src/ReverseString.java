import java.util.*;

public class ReverseString {
    public static void main(String[] args) {
        {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter s String: ");
            String str = sc.nextLine();
            StringBuilder sb = new StringBuilder(str);
            sb.reverse();
            System.out.println("The reverse of string is: " + sb);
        }

    }
}
